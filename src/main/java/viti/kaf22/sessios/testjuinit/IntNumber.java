package viti.kaf22.sessios.testjuinit;

public class IntNumber {

    private int value;


    public IntNumber() {
    }

    public IntNumber(int value) {
        this.value = value;
    }


    public void add(int number){
        value += number;
    }

    public void multiply(int number){
        value *= number;
    }

    public void division(int number){
        value /= number;
    }


    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
