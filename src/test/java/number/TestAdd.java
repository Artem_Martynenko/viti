package number;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import viti.kaf22.sessios.testjuinit.IntNumber;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RunWith(Parameterized.class)
public class TestAdd {



    private IntNumber number;




    @Before
    public void setUp() throws Exception {
        number = new IntNumber(5);

    }

    @After
    public void tearDown() throws Exception {

    }

    @BeforeClass
    public static void setUpClass(){

        System.out.println("Before class");
    }


    @AfterClass
    public static void tearDownClass(){

        System.out.println("after class");
    }


    @Test
    public void testAdd2(){
        number.add(2);
        Assert.assertEquals(7,number.getValue());
    }


    @Test
    public void testAdd3(){
        number.add(3);
        Assert.assertEquals(8,number.getValue());
    }






}
