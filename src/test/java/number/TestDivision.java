package number;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import viti.kaf22.sessios.testjuinit.IntNumber;

public class TestDivision {

    private IntNumber intNumber;


    @Before
    public void setUp() throws Exception {
        intNumber = new IntNumber(5);
    }


    @Test
    public void testSelfDivision() {
        intNumber.division(intNumber.getValue());
        Assert.assertEquals(1,intNumber.getValue());
    }


    @Test(expected = ArithmeticException.class)
    public void testZeroDivision() {
        intNumber.division(0);
    }
}
